package main

import (
	sump "GoTestCover/pkg/sump"
	"fmt"
)

func main() {
	a := 1
	b := 2
	fmt.Printf("%d + %d = %d", a, b, sump.Sump(a, b))
}

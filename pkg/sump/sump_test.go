package sump

import "testing"

func TestSump(t *testing.T) {
	type args struct {
		a int
		b int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{"1 + 1", args{1, 1}, 2},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Sump(tt.args.a, tt.args.b); got != tt.want {
				t.Errorf("Sump() = %v, want %v", got, tt.want)
			}
		})
	}
}

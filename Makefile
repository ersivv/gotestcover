coverage: ## Generate global code coverage report
	go test -cover -coverpkg=./... -coverprofile=.testCoverage.txt

coverhtml: ## Generate global code coverage report in HTML
	go tool cover -html=.testCoverage.txt